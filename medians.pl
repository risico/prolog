average(List, Result) :- length(List, Len), sum(List, Sum), Result is Sum / Len.

sum([], 0).
sum([H|T], Sum) :- sum(T, Temp), Sum is Temp + H.
